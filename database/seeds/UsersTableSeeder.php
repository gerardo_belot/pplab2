<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Gerardo Belot',
                'email' => 'gbelot2003@hotmail.com',
                'password' => '$2y$10$ijTs43mBdSWCytyFivN34uZLBrtsfPPYTEdw.1x69LQd13ZeQkiNe',
                'remember_token' => NULL,
                'created_at' => '2018-02-19 02:37:12',
                'updated_at' => '2018-02-19 02:37:12',
            ),
        ));
        
        
    }
}