<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        factory(App\Firma::class, 50)->create();
        factory(App\IdCategoria::class, 50)->create();
        factory(App\Plantilla::class, 10)->create();
        $this->call(PermissionsTableSeeder::class);
    }
}
