<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'manage-cito',
                'display_name' => 'Gestionar Citología',
                'description' => 'Permisos para gestionar citología',
                'created_at' => '2018-02-21 21:38:57',
                'updated_at' => '2018-02-21 21:38:58',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'manage-histo',
                'display_name' => 'Gestionar Histopatología	',
                'description' => 'Permisos para gestionar Hitopatología',
                'created_at' => '2018-02-21 21:39:22',
                'updated_at' => '2018-02-21 21:39:24',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'manage-ids	',
                'display_name' => 'Gestionar Firmas	',
                'description' => 'Permiso para crear firmas
',
                'created_at' => '2018-02-21 21:39:48',
                'updated_at' => '2018-02-21 21:39:50',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'generate-reportes',
                'display_name' => 'Ver Reportes',
                'description' => 'Permisos para acceder a reportes',
                'created_at' => '2018-02-21 21:40:16',
                'updated_at' => '2018-02-21 21:40:17',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'manage-rols',
                'display_name' => 'Administrar Roles',
                'description' => 'Permiso para administrar roles',
                'created_at' => '2018-02-21 21:40:43',
                'updated_at' => '2018-02-21 21:40:44',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'manage-templates',
                'display_name' => 'Gestionar Plantillas',
                'description' => 'Permiso para gestionar plantillas',
                'created_at' => '2018-02-21 21:41:11',
                'updated_at' => '2018-02-21 21:41:12',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'manage-firmas',
                'display_name' => 'Gestionar firmas',
                'description' => 'Permiso para gestionar firmas',
                'created_at' => '2018-02-21 21:41:35',
                'updated_at' => '2018-02-21 21:41:36',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'manage-users',
                'display_name' => 'Administrar Usuarios',
                'description' => 'Permiso para administrar usuarios',
                'created_at' => '2018-02-21 21:41:59',
                'updated_at' => '2018-02-21 21:42:00',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'show-fact',
                'display_name' => 'Ver Facturas',
                'description' => 'Permisos para revisar Facturas',
                'created_at' => '2018-02-21 21:43:48',
                'updated_at' => '2018-02-21 21:43:49',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'see-users',
                'display_name' => 'Ver usuarios',
                'description' => 'Permiso para acceder a index de usuarios, pero no editarlos',
                'created_at' => '2018-02-21 21:44:11',
                'updated_at' => '2018-02-21 21:44:12',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'show-logs',
                'display_name' => 'Ver registros de Logs',
                'description' => 'Permisos para registros de logs',
                'created_at' => '2018-02-21 21:44:36',
                'updated_at' => '2018-02-21 21:44:37',
            ),
        ));
        
        
    }
}