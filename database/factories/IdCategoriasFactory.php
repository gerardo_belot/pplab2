<?php

use Faker\Generator as Faker;

$factory->define(App\IdCategoria::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'status' => $faker->boolean,
    ];
});
