<?php

use Faker\Generator as Faker;

$factory->define(App\Plantilla::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'body' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'type' => $faker->numberBetween(1, 2), //Solo pueden ser 2 tipos
        'status' => $faker->boolean,
    ];
});