<?php

namespace Tests\Unit;

use Acme\Helpers\checkRequestStatus;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class checkRequeststatusTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

    /** @test */
    public function checkRequest_calculate_status_from_string_to_int()
    {
        $helper = new checkRequestStatus("on");

        self::assertEquals($helper->setCheckStatus(), 1);
    }
}
