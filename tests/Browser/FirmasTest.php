<?php

namespace Tests\Browser;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class FirmasTest extends DuskTestCase
{
    use DatabaseMigrations;


    /** @test */
    public function firmas_display_creation_modal()
    {

        $user = factory(User::class)->create([
            'email' => 'test@example.com',
            'password' => bcrypt('123456')
        ]);

        $this->browse(function (Browser $browser) use  ($user){

            $browser->visit('/login')
                ->type('email', $user->email)
                ->type('password', '123456')
                ->press('Login')
                ->assertPathIs('/home');

            $browser->clickLink('Firmas')
                ->assertPathIs('/firmas')
                ->clickLink('Add')
                ->waitFor('#CreateModal')
                ->whenAvailable('#CreateModal', function ($modal){
                    $modal
                        ->type('name', 'test')
                        ->type('collegiate', '123456')
                        ->type('extra', '32165')
                        ->press('Save changes');
                })
                ->assertPathIs('/firmas')
                ->assertSee('test')
                ->clickLink('test')
                ->waitFor("#myModal")
                ->whenAvailable("#myModal", function ($modal){
                    $modal
                        ->assertSee('Edición de test')
                        ->type("name", "tester")
                        ->press('Save changes');
                })
                ->assertPathIs('/firmas')
                ->assertSee('tester');

        });
    }

}











