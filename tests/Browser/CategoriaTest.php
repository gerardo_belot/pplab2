<?php

namespace Tests\Browser;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CategoriaTest extends DuskTestCase
{
    use DatabaseMigrations;


    /** @test */
    public function categorias_display_creation_modal()
    {
        $user = factory(User::class)->create([
            'email' => 'test@example.com',
            'password' => bcrypt('123456')
        ]);

        $this->browse(function (Browser $browser) use ($user) {

            $browser->visit('/login')
                ->type('email', $user->email)
                ->type('password', '123456')
                ->press('Login')
                ->assertPathIs('/home');

            $browser->clickLink('Categorias')
                ->assertPathIs('/categorias')
                ->clickLink('Add')
                ->waitFor('#CreateModal')
                ->whenAvailable('#CreateModal', function ($modal) {
                    $modal
                        ->type('name', 'test')
                        ->press('Save changes');
                })
                ->assertPathIs('/categorias')
                ->assertSee('test')
                ->clickLink('test')
                ->waitFor('#CatEditModal')
                ->whenAvailable('#CatEditModal', function ($modal){
                    $modal
                        ->assertSee('Edición de test')
                        ->type("name", "tester")
                        ->press("Save change");
                })
                ->assertPathIs('/categorias')
                ->assertSee('tester');

        });
    }
}
