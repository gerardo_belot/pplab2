<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class PlantillasTest extends DuskTestCase
{

    use DatabaseMigrations;

    /** @test */
    public function Plantillas_display_creation_modal()
    {

        $user = factory(User::class)->create([
            'email' => 'test@example.com',
            'password' => bcrypt('123456')
        ]);


        $this->browse(function (Browser $browser) use ($user) {

            $browser->visit('/login')
                ->type('email', $user->email)
                ->type('password', '123456')
                ->press('Login')
                ->assertPathIs('/home');

            $browser->clickLink('Plantillas')
                ->assertPathIs('/plantillas')
                ->clickLink('Add');

        });
    }
}
