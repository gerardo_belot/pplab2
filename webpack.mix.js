let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');
    //.st('datatable.net-bs/css/dataTables.bootstrap.css', 'public/all.css');

mix.styles([
    'resources/assets/css/dataTables.bootstrap.css',
    'resources/assets/css/bootstrap2-toggle.css',
], 'public/css/all.css');