<?php
namespace Acme\Helpers;

use Illuminate\Http\Request;

class checkRequestStatus
{

    private $status;
    /**
     * checkRequestStatus constructor.
     * @param $status
     */
    public function __construct($status)
    {
        $this->status = $status;
    }

    public function setCheckStatus()
    {
        return $this->checkRequestStatus();
    }

    /**
     * @param $status
     * @return int
     */
    private function checkRequestStatus()
    {
        if ($this->status == 'on'):
            $stat = 1;
        else:
            $stat = 0;
        endif;

        return $stat;
    }
}