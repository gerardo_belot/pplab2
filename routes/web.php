<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('firmas', 'FirmaController');
Route::get('firma/data-array', 'DataTables\FirmasDtController@index');
Route::get('firma/state/{id}/{state}', 'DataTables\FirmasDtController@state');

Route::resource('categorias', 'IdCategoriaController');
Route::get('categoria/data-array', 'DataTables\IdCategoriasDtController@index');
Route::get('categoria/state/{id}/{state}', 'DataTables\IdCategoriasDtController@state');

Route::resource('plantillas', 'PlantillaController');
Route::get('plantilla/data-array', 'DataTables\PlantillasDtcontroller@index');
Route::get('plantilla/state/{id}/{state}', 'DataTables\PlantillasDtcontroller@state');
