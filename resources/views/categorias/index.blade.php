@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="text-muted pull-right">
                <h2>Categorias
                    <a name="Add" href="/categorias" class="btn btn-info addNewIdCategory" alt="Crear Citologia">Add</a>
                </h2>
            </div>
        </div>
        <hr />
        <div class="col-md-12">
            <table id="dataTableCategori" class="table table-bordered">
                <thead>
                <tr>
                    <th>No.</th>
                    <th>nombre</th>
                    <th>Estado</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>

    </div>
    @include('categorias.modal')
    @include('categorias.modal_create')
@stop