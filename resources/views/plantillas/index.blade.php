@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="text-muted pull-right">
                <h2>Plantillas
                    <a href="/plantillas" class="btn btn-info addNew" alt="Crear Plantillas">Add</a>
                </h2>
            </div>
        </div>
        <hr />
        <div class="col-md-12">
            <table id="dataTablePlantillas" class="table table-bordered">
                <thead>
                <tr>
                    <th>No.</th>
                    <th>nombre</th>
                    <th>Tipo</th>
                    <th>Estado</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>

    </div>

@stop