<div class="row">
    <div class="col-md-12 form-group box-style">
        <label for="nombre">Nombre de Plantillas</label>
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12  form-group box-style">
        <label for="plantillaa">Plantillas</label>
        {!! Form::textarea('body', null, ['class' => 'form-control ckeditor', 'id' => 'editor']) !!}
    </div>
</div>

<div class="row">

    <div class="col-md-3 box-style">
        <label for="state">Estado</label>
        <br>
        <div class="control">
            {!! Form::checkbox('status', null , 1,
           [
           'data-toggle' => 'toggle',
           'data-on' => 'Activo',
           'data-off' => 'Desactivado',
           'class' => 'emiter'
           ])
            !!}
        </div>
        <div class="lcontrol">
            <span class="sing glyphicon"></span>
        </div>
    </div>

    <div class="col-md-6 col-md-push-3 form-group box-style">
        <label for="tipo">Plantillas</label>
        {!! Form::select('type', ['1' => 'Biopsias', '2' => 'Certificados'], null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row">

</div>