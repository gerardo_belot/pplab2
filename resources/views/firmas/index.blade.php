@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="text-muted pull-right">
                <h2>Firmas
                    <a dusk="modalButton" href="/firmas" class="btn btn-info addNew" alt="Crear Citologia">Add</a>
                </h2>
            </div>
        </div>
        <hr />
        <div class="col-md-12">
            <table id="dataTable" class="table table-bordered">
                <thead>
                <tr>
                    <th>No.</th>
                    <th>nombre</th>
                    <th>No. de Colegiado</th>
                    <th>Estado</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>

    </div>
    @include('firmas.modal')
    @include('firmas.modal_create')
@stop