import * as Toastr from 'toastr';

function CategoriasModalEdit(url) {
    $.get(url)
        .done(function (data) {
            let title = `Edición de <strong>${data.name}</strong>`;
            $('#CatEditModal').modal();
            $('#CatEditModal .modal-title').html(title);
            $('#CatEditModal #name').val(data.name);
        })
        .fail(function (data) {
            var status = data.status;
            var statusText = data.statusText;
            Toastr.error(`Hubo algun problema, el servidor tiene "Status:" ${status}, mensaje de error es: ${statusText}`);
        });

    $('#CatEditModal #submit').on('click', function () {
        let categoria = {};
        categoria.name = $('#CatEditModal #name').val();
        axios.patch(url, categoria)
            .then(function (data) {
                $('#CatEditModal').modal('hide');
                var table = $('#dataTableCategori').DataTable();
                table.ajax.reload();

                categoria = undefined;
                $('#CatEditModal #name').val("");
                url = undefined;
            }).catch(function (error) {
                console.log(error);
            })
    })
}

export default CategoriasModalEdit;