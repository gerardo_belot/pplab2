import * as Toastr from 'toastr';

function CategoriasModalCreate(url) {

    let title = `Creación de Nuevo Registro`;
    $('#CreateModal').modal();
    $('#CreateModal .modal-title').html(title);
    $('#CreateModal #name').val("");

    $('#CreateModal #submit').on('click', function (e) {
        let categorias = {};
        categorias.name = $('#CreateModal #name').val();
        categorias.status = true;
        axios.post(url, categorias)
            .then(function (data) {
                $('#CreateModal').modal('hide');
                var table = $('#dataTableCategori').DataTable();
                table.ajax.reload();

                categorias = undefined;
                url = undefined;
            }).catch(function (error) {
            console.log(error);
        })
    })

}

export default CategoriasModalCreate;