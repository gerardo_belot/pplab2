import CategoriasSendDataToServer from './CategoriasSendDataToServer';
import CategoriasModalEdit from './CategoriasModalEdit';
import CategoriasModalCreate from './CategoriasModalCreate';

$(document).ready(function () {
    $('.addNewIdCategory').on('click', function (e) {
        e.preventDefault();
        let url = $(this).attr('href');
        console.log(url);
        CategoriasModalCreate(url)
    })

    $('#dataTableCategori').dataTable({
        serverSide: true,
        processing: true,
        "order": [[0, "desc"]],
        ajax: 'categoria/data-array',
        "fnDrawCallback": function () {
            $('.catEdit').on('click', function (e) {
                e.preventDefault();
                let url = $(this).attr('href');
                CategoriasModalEdit(url);
            });

            jQuery('.emitCategori').bootstrapToggle();

            $('.emitCategori').on('change', function () {
                let id = $(this).attr('id');
                let ch = $(this).is(':checked');
                let url = 'categoria/state';

                CategoriasSendDataToServer(id, ch, url);
            })
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'status', name: 'status'},
        ],
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Registro no encotrado - lo sentimos",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros de esa busqueda",
            "infoFiltered": "(filtrado de _MAX_ total Total de regístros)",
            "search": "Busqueda:",
            "processing": "Procesando",
            "loadingRecords": "Cargando...",

            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            },
            "aria": {
                "sortAscending": ": Ordenando por columnas asendente",
                "sortDescending": ": Ordenando por columnas desendente"
            },
        }
    });
});