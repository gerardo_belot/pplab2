import * as Toastr from 'toastr';

function OpenCreateModal(url){

    let title = `Creación de Nuevo Registro`;
    $('#CreateModal').modal();
    $('#CreateModal .modal-title').html(title);
    $('#CreateModal #name').val("");
    $('#CreateModal #collegiate').val("");
    $('#CreateModal #extra').val("");


    $('#CreateModal #submit').on('click', function () {
        let firma = {};
        firma.name = $('#CreateModal #name').val();
        firma.collegiate = $('#CreateModal #collegiate').val();
        firma.extra = $('#myCreateModalModal #extra').val();
        firma.status = 0;
        console.log(firma);
        axios.post(url, firma)
            .then(function (data) {
                $('#CreateModal').modal('hide');
                var table = $('#dataTable').DataTable();
                table.ajax.reload();

                firma = undefined;
                url = undefined;
            }).catch(function (error) {
            console.log(error);
        });
    });
}

export default OpenCreateModal;