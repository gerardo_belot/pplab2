import sendDataToServer from './FirmasTs';
import OpenEditModal from './FirmasMdl';
import OpenCreateModal from './FirmasMdCreate';

$(document).ready(function () {

    $('.addNew').on('click', function (e) {
        let url = $(this).attr('href');
        e.preventDefault();
        OpenCreateModal(url);
    });

    $('#dataTable').dataTable({
        serverSide: true,
        processing: true,
        "order": [[0, "desc"]],
        ajax: 'firma/data-array',
        "fnDrawCallback": function () {
            $('.exe').on('click', function (e) {
                e.preventDefault();
                let url = $(this).attr('href');
                OpenEditModal(url);
            });

            jQuery('.emit').bootstrapToggle();

            $('.emit').on('change', function () {
                let id = $(this).attr('id');
                let ch = $(this).is(':checked');
                let url = 'firma/state';

                sendDataToServer(id, ch, url);
            })
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'collegiate', name: 'collegiate'},
            {data: 'state', name: 'state'},
        ],
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Registro no encotrado - lo sentimos",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros de esa busqueda",
            "infoFiltered": "(filtrado de _MAX_ total Total de regístros)",
            "search": "Busqueda:",
            "processing": "Procesando",
            "loadingRecords": "Cargando...",

            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            },
            "aria": {
                "sortAscending": ": Ordenando por columnas asendente",
                "sortDescending": ": Ordenando por columnas desendente"
            },
        }
    });

});