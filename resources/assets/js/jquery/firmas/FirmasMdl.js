import * as Toastr from 'toastr';

function OpenEditModal(url) {
    $.get(url)
        .done(function (data) {
            let title = `Edición de <strong>${data.name}</strong>`;
            $('#myModal').modal();
            $('#myModal .modal-title').html(title);
            $('#myModal #name').val(data.name);
            $('#myModal #collegiate').val(data.collegiate);
            $('#myModal #extra').val(data.extra);
        }).fail(function (data) {
            var status = data.status;
            var statusText = data.statusText;
            Toastr.error(`Hubo algun problema, el servidor tiene "Status:" ${status}, mensaje de error es: ${statusText}`);
        }
    );

    $('#myModal #submit').on('click', function () {
        let firma = {};
        firma.name = $('#myModal #name').val();
        firma.collegiate = $('#myModal #collegiate').val();
        firma.extra = $('#myModal #extra').val();
        console.log(firma);
        axios.patch(url, firma)
            .then(function (data) {
                $('#myModal').modal('hide');
                var table = $('#dataTable').DataTable();
                table.ajax.reload();

                firma = undefined;
                $('#myModal #name').val("");
                $('#myModal #collegiate').val("");
                $('#myModal #extra').val("");
                url = undefined;

            }).catch(function (error) {
            console.log(error);
        });
    });
}

export default OpenEditModal;