import * as Toastr from 'toastr';

function sendDataToServer(id, ch, url) {
    if (ch === true) {
        let state = 1;
        $.get(url + '/' + id + '/' + state)
            .done(function (data) {
                Toastr.success(`El estado de <strong>${data.name}</strong> a cambiado a Activo`, 'Registro actualizado', {timeOut: 5000});
            })
            .fail(function (data) {
                var status = data.status;
                var statusText = data.statusText;
                Toastr.error(`Hubo algun problema, el servidor tiene "Status:" ${status}, mensaje de error es: ${statusText}`);
            });

    } else {
        let state = 0;
        $.get(url + '/' + id + '/' + state)
            .done(function (data) {
                Toastr.warning(`El estado de <strong>${data.name}</strong> a cambiado a Desactivado`, 'Registro actualizado', {timeOut: 5000});
            })
            .fail(function (data) {
                var status = data.status;
                var statusText = data.statusText;
                Toastr.error(`Hubo algun problema, el servidor tiene "Status:" ${status}, mensaje de error es: ${statusText}`);
            });
    }
}

export default sendDataToServer;