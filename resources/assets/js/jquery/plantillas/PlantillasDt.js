import PlantillasSendData from './PlantillasSendData';

$(document).ready(function () {



    $('#dataTablePlantillas').DataTable({
        processing: true,
        serverSide: true,
        "order": [[0, "desc"]],
        ajax: 'plantilla/data-array',
        "fnDrawCallback": function (){
            jQuery('.emitPlantillas').bootstrapToggle();
            $('.emitPlantillas').on('change', function () {
                let id = $(this).attr('id');
                let ch = $(this).is(':checked');
                let url = 'plantilla/state';

                PlantillasSendData(id, ch, url);
            })
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'type', name: 'type'},
            {data: 'status', name: 'status'},
        ],
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Registro no encotrado - lo sentimos",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros de esa busqueda",
            "infoFiltered": "(filtrado de _MAX_ total Total de regístros)",
            "search": "Busqueda:",
            "processing": "Procesando",
            "loadingRecords": "Cargando...",

            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            },
            "aria": {
                "sortAscending": ": Ordenando por columnas asendente",
                "sortDescending": ": Ordenando por columnas desendente"
            },
        }
    });
});