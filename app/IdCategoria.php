<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IdCategoria extends Model
{

    /**
     * @var string
     */
    protected $table = 'categorias';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'status'
    ];

    /**
     * Casting Booleans
     */
    protected $casts = [
        'status' => 'boolean'
    ];

}
