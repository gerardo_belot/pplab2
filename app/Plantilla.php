<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plantilla extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'body', 'type', 'status'];
}

