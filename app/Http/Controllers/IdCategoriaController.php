<?php

namespace App\Http\Controllers;

use App\IdCategoria;
use Illuminate\Http\Request;

class IdCategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View('categorias.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = IdCategoria::create($request->all());
        return $item;
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Firma $firma
     */
    public function show($id)
    {
        $item = IdCategoria::findOrFail($id);
        return $item;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param IdCategoria $categoria
     * @return \Illuminate\Http\Response
     * @internal param \App\Firma $firma
     */
    public function update(Request $request, IdCategoria $categoria)
    {
        $categoria->update($request->all());
        return $categoria;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Firma $firma
     * @return \Illuminate\Http\Response
     */
    public function destroy(IdCategoria $firma)
    {
        //
    }
}
