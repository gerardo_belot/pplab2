<?php

namespace App\Http\Controllers;

use Acme\Helpers\checkRequestStatus;
use App\Plantilla;
use Illuminate\Http\Request;

class PlantillaController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return View('plantillas.index');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $item = Plantilla::findOrFail($id);
        return View('plantillas.edit', compact('item'));
    }

    /**
     * @param Plantilla $plantilla
     * @return Plantilla
     */
    public function store(Plantilla $plantilla)
    {
        return $plantilla;
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $plantilla = Plantilla::findOrFail($id);

        $helper = new checkRequestStatus($request['status']);
        $request['status'] = $helper->setCheckStatus();

        $plantilla->update($request->all());
        return $plantilla;
    }


}
