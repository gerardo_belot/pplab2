<?php

namespace App\Http\Controllers\DataTables;

use Yajra\Datatables\Datatables;

use App\Firma;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FirmasDtController extends Controller
{
    public function index(Datatables $datatables)
    {
        $builder = Firma::query()->select('id', 'name', 'collegiate', 'extra', 'status');

        return $datatables->eloquent($builder)
            ->editColumn('name', function ($firma) {
                return '<a class="exe" href="firmas/' . $firma->id . '">' . $firma->name . '</a>';
            })
            ->addColumn('state', function ($firma) {
                if ($firma->status == true) {
                    $checked = 'checked';
                } else {
                    $checked = '';
                }
                return '<input id="' . $firma->id . '" type="checkbox" data-on="Activo" data-off="Desactivado" class="emit" ' . $checked . ' />';
            })
            ->rawColumns(['name', 'state'])
            ->make(true);
    }

    public function state($id, $state)
    {
        $item = Firma::findOrFail($id);
        $item->status = $state;
        $item->update();
        return $item;
    }
}
