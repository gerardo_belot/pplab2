<?php

namespace App\Http\Controllers\DataTables;

use App\IdCategoria;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class IdCategoriasDtController extends Controller
{
   public function index(Datatables $datatables)
   {
       $builder = IdCategoria::query()->select('id', 'name',  'status');

       return $datatables->eloquent($builder)
           ->editColumn('name', function ($IdCategorias) {
               return '<a class="catEdit" href="categorias/' . $IdCategorias->id . '">' . $IdCategorias->name . '</a>';
           })
           ->addColumn('status', function ($IdCategorias) {
               if ($IdCategorias->status == true) {
                   $checked = 'checked';
               } else {
                   $checked = '';
               }
               return '<input id="' . $IdCategorias->id . '" type="checkbox" data-on="Activo" data-off="Desactivado" class="emitCategori" ' . $checked . ' />';
           })
           ->rawColumns(['name', 'status'])
           ->make(true);
   }

    public function state($id, $state)
    {
        $item = IdCategoria::findOrFail($id);
        $item->status = $state;
        $item->update();
        return $item;
    }
}
