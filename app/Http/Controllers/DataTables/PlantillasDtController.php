<?php

namespace App\Http\Controllers\DataTables;

use App\Plantilla;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class PlantillasDtController extends Controller
{
    public function index(Datatables $dataTable)
    {
        $builder = Plantilla::query()->select('id', 'name', 'type', 'status');

        return $dataTable->eloquent($builder)
            ->editColumn('name', function ($plantillas){
                return '<a class="plantEdit" href="plantillas/' . $plantillas->id . '">' . $plantillas->name . '</a>';
            })
            ->editColumn('type', function ($plantillas){
                if($plantillas->type == 1 ){$type = 'Biopsias';} else{$type = 'Certificados';}
                return $type;
            })
            ->addColumn('status', function ($plantillas) {
                if ($plantillas->status == true) {
                    $checked = 'checked';
                } else {
                    $checked = '';
                }
                return '<input id="' . $plantillas->id . '" type="checkbox" data-on="Activo" data-off="Desactivado" class="emitPlantillas" ' . $checked . ' />';
            })
            ->rawColumns(['name', 'type', 'status'])
            ->make(true);
    }

    public function state($id, $state)
    {
        $item = Plantilla::findOrFail($id);
        $item->status = $state;
        $item->update();
        return $item;
    }
}
