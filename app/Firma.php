<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Firma extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'collegiate', 'extra', 'status'
    ];

    /**
     * Casting Booleans
     */
    protected $casts = [
      'status' => 'boolean'
    ];

}
